//
//  ViewController.swift
//  TestLocationGeoFence
//
//  Created by Fereizqo Sulaiman on 03/12/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol HandleMapSearch {
    func dropPinZoomIn(placemark: MKPlacemark)
}

struct PreferencesKeys {
  static let savedItems = "savedItems"
}

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var radiusSlider: UISlider!
    @IBOutlet weak var radiusLabel: UILabel!
    
    var radius: Double = 500
    var resultSearchController: UISearchController? = nil
    var selectedPin: MKPlacemark? = nil
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Appearances
        radiusSlider.value = Float(radius)
        radiusSlider.isEnabled = false
        radiusSlider.isHidden = true
        
        // Setup search result table
        let locationSearchTable = storyboard?.instantiateViewController(identifier: "LocationSeachTableViewController") as! LocationSeachTableViewController
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController?.searchResultsUpdater = locationSearchTable
        locationSearchTable.mapView = mapView
        locationSearchTable.handleMapSearchDelegate = self
        
        // Setup search bar
        let searchBar = resultSearchController?.searchBar
        searchBar?.sizeToFit()
        searchBar?.placeholder = "Search for place"
        navigationItem.titleView = resultSearchController?.searchBar
        
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        definesPresentationContext = true
        
        
        // Gesture recognize
        let longPress: UILongPressGestureRecognizer = UILongPressGestureRecognizer()
        longPress.addTarget(self, action: #selector(addAnnotationOnLongPress(gesture:)))
        longPress.minimumPressDuration = 0.5
        mapView.addGestureRecognizer(longPress)
        
        // Location manager
        if CLLocationManager.locationServicesEnabled() {
            mapView.delegate = self
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.distanceFilter = 10
            locationManager.allowsBackgroundLocationUpdates = true
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
        }
    }
    
    @IBAction func changeRadiusSlider(_ sender: UISlider) {
        radius = Double(radiusSlider.value)
        radiusLabel.text = "\(radius) meter"
        
        makeRadiusCircle(location: (selectedPin?.location!)!, size: radius)
        setupGeofence(location: (selectedPin?.location!)!, size: radius)
    }
    
    @objc
    func addAnnotationOnLongPress(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .ended {
            let point = gesture.location(in: self.mapView)
            let coordinate = self.mapView.convert(point, toCoordinateFrom: self.mapView)
            print("Long press coordinate: \(coordinate)")
            
            mapView.removeAnnotations(mapView.annotations)
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            
            geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
                if error == nil {
                    let placemark = MKPlacemark(placemark: placemarks![0])
                    self.dropPinZoomIn(placemark: placemark)
                } else {
                    print("Reverse geocode fail: \(error?.localizedDescription ?? "Error could not reverse geocode")")
                }
            }
            
//            let annotation = MKPointAnnotation()
//            annotation.coordinate = coordinate
//            annotation.title = "Pinned Location"
//            mapView.addAnnotation(annotation)
        }
    }
    
    func makeRadiusCircle(location: CLLocation, size: Double) {
        self.mapView.removeOverlays(self.mapView.overlays)
        
        let circle = MKCircle(center: location.coordinate, radius: size)
        self.mapView.addOverlay(circle)
    }
    
    func setupGeofence(location: CLLocation, size: Double) {
        let geofenceRegion = CLCircularRegion(center: location.coordinate, radius: size, identifier: "Destination")
        geofenceRegion.notifyOnEntry = true
        
        self.locationManager.startMonitoring(for: geofenceRegion)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = .red
            circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        } else {
            return MKOverlayRenderer()
        }
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        print("Latitude: \(locations.last?.coordinate.latitude)")
//        print("Longitude: \(locations.last?.coordinate.longitude)")
//        if let location = locations.first {
//            print("Location: \(location)")
//        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
//        print("You are inside your region")
    }
}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let myPinIdentifier = "PinAnnotationIdentifier"
        
        // Generate pin
        let myPinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: myPinIdentifier)
        
        myPinView.animatesDrop = true
//        myPinView.canShowCallout = true
//        myPinView.image = UIImage(named: "")
        myPinView.annotation = annotation
        
        return myPinView
    }
}

extension ViewController: HandleMapSearch {
    func dropPinZoomIn(placemark: MKPlacemark) {
        // cache the pin
        selectedPin = placemark
        // clear existing pin
        mapView.removeAnnotations(mapView.annotations)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        if let city = placemark.locality,
            let state = placemark.administrativeArea {
            annotation.subtitle = "\(city) \(state)"
        }
        mapView.addAnnotation(annotation)
        
        let center = CLLocationCoordinate2D(latitude: (placemark.coordinate.latitude+(locationManager.location?.coordinate.latitude)!)/2, longitude: (placemark.coordinate.longitude+(locationManager.location?.coordinate.longitude)!)/2)
        
        let region = MKCoordinateRegion(center: center, latitudinalMeters: (placemark.location?.distance(from: locationManager.location!))!*1.5, longitudinalMeters: (placemark.location?.distance(from: locationManager.location!))!*1.5)
        
        mapView.setRegion(region, animated: true)
        radiusSlider.isEnabled = true
        radiusSlider.isHidden = false
    }
}


