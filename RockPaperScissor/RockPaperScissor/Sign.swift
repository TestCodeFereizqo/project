//
//  Sign.swift
//  RockPaperScissor
//
//  Created by Fereizqo Sulaiman on 15/05/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import Foundation
import GameplayKit

let randomChoice = GKRandomDistribution(lowestValue: 0, highestValue: 2)

func randomSign() -> Sign {
    let sign = randomChoice.nextInt()
    if sign == 0 {
        return Sign.rock
    } else if sign == 1 {
        return Sign.paper
    } else {
        return Sign.scissors
    }
}


enum Sign {
    case rock, paper, scissors
    
    var emoji : String {
        switch self {
        case .rock:
            return "👊🏼"
        case .paper:
            return "🖐🏼"
        case .scissors:
            return "✌🏼"
        }
    }
    
    func takeTurn(opponet: Sign) -> GameState {
        switch self {
        case .rock:
            switch opponet {
            case .rock :
                return GameState.draw
            case .paper :
                return GameState.lose
            case .scissors :
                return GameState.win
            }
        case .paper:
            switch opponet {
            case .rock :
                return GameState.win
            case .paper :
                return GameState.draw
            case .scissors :
                return GameState.lose
            }
        case .scissors :
            switch opponet {
            case .rock :
                return GameState.lose
            case .paper :
                return GameState.win
            case .scissors :
                return GameState.draw
            }
        }
    }
}
