//
//  GameState.swift
//  RockPaperScissor
//
//  Created by Fereizqo Sulaiman on 15/05/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import Foundation

enum GameState {
    case start, win, lose, draw
}
