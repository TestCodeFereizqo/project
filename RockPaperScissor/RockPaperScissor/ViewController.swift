//
//  ViewController.swift
//  RockPaperScissor
//
//  Created by Fereizqo Sulaiman on 15/05/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var appSignLabel: UILabel!
    @IBOutlet weak var statusGameLabel: UILabel!
    @IBOutlet weak var rockButton: UIButton!
    @IBOutlet weak var paperButton: UIButton!
    @IBOutlet weak var scissorsButton: UIButton!
    @IBOutlet weak var playAgainButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        reset()
    }
    
    func reset() {
        appSignLabel.text = "🤖"
        statusGameLabel.text = "Rock, paper, scissors?"
        playAgainButton.isHidden = true
        rockButton.isHidden = false
        paperButton.isHidden = false
        scissorsButton.isHidden = false
        rockButton.isEnabled = true
        paperButton.isEnabled = true
        scissorsButton.isEnabled = true
    }
    
    func play (playerTurn: Sign) {
        rockButton.isEnabled = false
        paperButton.isEnabled = false
        scissorsButton.isEnabled = false
        
        let opponets = randomSign()
        appSignLabel.text = opponets.emoji
        
        let gameResult = playerTurn.takeTurn(opponet: opponets)
        
        switch gameResult {
        case .draw:
            statusGameLabel.text = "It's a draw"
        case .lose:
            statusGameLabel.text = "Sorry, you lose"
        case .win:
            statusGameLabel.text = "Horay, You Win!"
        case .start:
            statusGameLabel.text = "Rock, paper, scissors?"
        }
        
        switch playerTurn {
        case .rock:
            rockButton.isHidden = false
            paperButton.isHidden = true
            scissorsButton.isHidden = true
        case .paper:
            rockButton.isHidden = true
            paperButton.isHidden = false
            scissorsButton.isHidden = true
        case .scissors:
            rockButton.isHidden = true
            paperButton.isHidden = true
            scissorsButton.isHidden = false
        }
        playAgainButton.isHidden = false
    }

    @IBAction func rockButtonTap(_ sender: UIButton) {
        play(playerTurn: Sign.rock)
    }
    
    @IBAction func paperButtonTap(_ sender: UIButton) {
        play(playerTurn: .paper)
    }
    
    @IBAction func scissorsButtonTap(_ sender: UIButton) {
        play(playerTurn: .scissors)
    }
    
    @IBAction func playAgainButtonTap(_ sender: UIButton) {
        reset()
    }
}

