//
//  ViewController.swift
//  FunctionalAboutClosure
//
//  Created by Fereizqo Sulaiman on 13/01/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let someView = setupView()
        
        let someOtherView = setupViewUsingClosure()
    }
    
    func setupView() -> UIView {
        let view = UIView()
        view.backgroundColor = .red
        
        return view
    }
    
    let setupViewUsingClosure = { () -> UIView in
        let view = UIView()
        view.backgroundColor = .green
        
        return view
    }
    
    let setupViewUsingClosureShort: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        
        return view
    }() // IMPORTANT, added () at the end


}

