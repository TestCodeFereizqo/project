//
//  ViewController.swift
//  StructCar
//
//  Created by Fereizqo Sulaiman on 25/04/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    struct Color {
        var red : Int
        var green : Int
        var blue : Int
    }
    
    struct Size {
        var length : Int
        var width : Int
        var heigth : Int
    }
    
    struct Car {
        var nama : String
        var brand : String
        var warna : Color
        var dimension : Size
    }
    
    let kuning = Color(red: 1, green: 1, blue: 0)
    let merah = Color(red: 1, green: 0, blue: 0)
    let hijau = Color(red: 0, green: 1, blue: 0)
    
    let smallSize = Size(length: 120, width: 200, heigth: 100)
    let mediumSize = Size(length: 200, width: 210, heigth: 120)
    let bigSize = Size(length: 340, width: 240, heigth: 150)
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mobilCell", for: indexPath)
        
        var warna = [kuning,merah,hijau]
        var ukuran = [smallSize,mediumSize,bigSize]
        
        let car1 = Car(nama: "Brio", brand: "Honda", warna: kuning, dimension: Size(length: 100, width: 10, heigth: 100))
        
        return cell
    }
    

}

