//
//  mobilTableViewCell.swift
//  StructCar
//
//  Created by Fereizqo Sulaiman on 25/04/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

class mobilTableViewCell: UITableViewCell {

    @IBOutlet weak var mobilGambarView: UIView!
    @IBOutlet weak var namaMobilLabel: UILabel!
    @IBOutlet weak var brandMobilLabel: UILabel!
    @IBOutlet weak var warnaMobilLabel: UILabel!
    @IBOutlet weak var sizeMobilLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
