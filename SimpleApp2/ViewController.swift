//
//  ViewController.swift
//  Coba
//
//  Created by Fereizqo Sulaiman on 22/03/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let Hewan : String = "Harimau"
    
    
    @IBOutlet weak var KnockLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        KnockLabel.text="Selamat Pagi"
    }

    @IBAction func TombolButton(_ sender: UIButton) {
        KnockLabel.text = Hewan
        KnockLabel.textColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
    }
}

