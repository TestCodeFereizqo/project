//
//  Helper.swift
//  CoreDataDemo
//
//  Created by Fereizqo Sulaiman on 22/08/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import CoreData

class Helper {
    
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    static let context = appDelegate.persistentContainer.viewContext
    
    static func createData(EntityName: String, Value: String, Key: String) {
        let userEntity = NSEntityDescription.entity(forEntityName: EntityName, in: context)
        
        let newUser = NSManagedObject(entity: userEntity!, insertInto: context)
        newUser.setValue(Value, forKey: Key)
        
        do {
            try context.save()
            
        } catch let error as NSError {
            print("Failed. \(error), \(error.userInfo) ")
        }
    }
    
    static func retrieveData(EntityName: String, Key: String) -> [String] {
        var entityData: [NSManagedObject] = []
        var retrieve: [String] = []
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityName)
        
        do {
            entityData = try context.fetch(fetchRequest) as! [NSManagedObject]
            for data in entityData {
                retrieve.append(data.value(forKey: Key) as! String)
                print(data.value(forKey: Key) as! String)
            }
        } catch  {
            print("Failed")
        }
        return retrieve
    }
}
