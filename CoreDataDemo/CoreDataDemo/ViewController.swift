//
//  ViewController.swift
//  CoreDataDemo
//
//  Created by Fereizqo Sulaiman on 04/07/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        createData1()
        retrieveData2()
    }
    
    func createData1() {
        Helper.createData(EntityName: "Users", Value: "Adi", Key: "username")
        Helper.createData(EntityName: "Users", Value: "Isa", Key: "username")
    }
    
    func retrieveData2() {
        let a = Helper.retrieveData(EntityName: "Users", Key: "username")
        print(a)
    }
    
    func createData() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let userEntity = NSEntityDescription.entity(forEntityName: "Users", in: context)
        
        let newUser = NSManagedObject(entity: userEntity!, insertInto: context)
        newUser.setValue("Adi", forKey: "username")
        newUser.setValue("1234", forKey: "password")
        newUser.setValue("17", forKey: "age")
        
        do {
            try context.save()
        } catch let error as NSError {
            print("Failed. \(error), \(error.userInfo) ")
        }
    }
    
    func retrieveData() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        
        do {
            let result = try context.fetch(fetchRequest)
            for data in result as! [NSManagedObject] {
                print(data.value(forKey: "age") as! String)
            }
        } catch {
            print("Failed.")
        }
    }


}

