//
//  GraphViewController.swift
//  CoDriver
//
//  Created by Fereizqo Sulaiman on 20/11/19.
//  Copyright © 2019 Indra Tirta Nugraha. All rights reserved.
//

import UIKit
import Charts

class GraphViewController: UIViewController {

    @IBOutlet weak var timeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var barView: BarChartView!
    @IBOutlet weak var highlightBackView: UIView!
    @IBOutlet weak var highlightDescLabel: UILabel!
    
    let monthComponents = Calendar.current.shortMonthSymbols
    let dayComponents = Calendar.current.shortWeekdaySymbols
    var dayArray:[String] = []
    var chooseDate: Date? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        highlightBackView.layer.cornerRadius = 10
        
        dayLabel.text = "Today"
        scoreLabel.text = ""
        for i in 0..<24 {
            if i < 10 {
                dayArray.append("0\(i)")
            } else {
                dayArray.append("\(i)")
            }
        }
        let numbers = generateRandomNumber(count: dayArray.count)
        barChart(dataEntryX: dayArray, dataEntryY: numbers, xAxisName: dayArray, separator: 7)
        scoreLabel.text = "\(average(numbers: numbers))"
        dayArray.removeAll()
        
    }
    
    // Calculate date
    func getDate(value: Int) -> Dictionary<String,[Int]> {
        let date = chooseDate!

        let calendar = Calendar.current
        let pastCalendar = Calendar.current.date(byAdding: .day, value: value, to: date)
        let pastDate = [calendar.component(.day, from: pastCalendar!),
                        calendar.component(.month, from: pastCalendar!),
                        calendar.component(.year, from: pastCalendar!)]
            
//        let currentDate = [format.string(from: date)]
        let currentDate = [calendar.component(.day, from: date),
                            calendar.component(.month, from: date),
                            calendar.component(.year, from: date),
                            calendar.component(.weekday, from: date)]

        let collectDate = ["Past": pastDate, "Now": currentDate]
        return collectDate
    }
        
    func barChart(dataEntryX entryX:[String], dataEntryY entryY:[Int], xAxisName xName:[String], separator: Double) {
        // Load data
        var dataEntry: [BarChartDataEntry] = []
        for i in 0...entryX.count-1 {
            let entry = BarChartDataEntry(x: Double(i), y: Double(entryY[i]))
            dataEntry.append(entry)
        }
        
        let dataSet = BarChartDataSet(entries: dataEntry, label: "")
        let data = BarChartData(dataSets: [dataSet])
        barView.data = data
            
        dataSet.colors = [UIColor(red: 247/255, green: 196/255, blue: 67/255, alpha: 1)]
            
        barChartAppearance(xAxisName: xName, separator: separator )
        barView.notifyDataSetChanged()
    }
        
    func barChartAppearance(xAxisName: [String], separator: Double) {
        let legend = barView.legend
        legend.enabled = false
                
        let xAxis = barView.xAxis
        xAxis.drawGridLinesEnabled = true
        xAxis.gridColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        xAxis.labelTextColor = .label
        xAxis.labelPosition = .bottom
        xAxis.valueFormatter = IndexAxisValueFormatter(values: xAxisName)
        xAxis.granularity = separator
                
        let yLeftAxis = barView.leftAxis
        yLeftAxis.gridColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        yLeftAxis.drawLabelsEnabled = false
                
        let yRightAxis = barView.rightAxis
        yRightAxis.gridColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        yRightAxis.labelTextColor = .label
            
        barView.borderLineWidth = 1
        barView.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                
        barView.animate(yAxisDuration: 0.2, easingOption: .linear)
    }
    
    @IBAction func switchTimeSegmentedControl(_ sender: UISegmentedControl) {
        switch timeSegmentedControl.selectedSegmentIndex {
        case 0:
            dayLabel.text = "Today"
            for i in 0..<24 {
                if i < 10 {
                    dayArray.append("0\(i)")
                } else {
                    dayArray.append("\(i)")
                }
            }
            
            let numbers = generateRandomNumber(count: dayArray.count)
            scoreLabel.text = "\(average(numbers: numbers))"
            barChart(dataEntryX: dayArray, dataEntryY: numbers, xAxisName: dayArray, separator: 7)
            dayArray.removeAll()
            break
            
        case 1:
            let date = getDate(value: -7)
            let pastNum = date["Past"]![0]
            let currentNum = date["Now"]![0]
            let currentMonth = date["Now"]![1]-1
            let currentYear = date["Now"]![2]
            let currentWeekday = date["Now"]![3]
            
            for i in 0...currentWeekday-1 {
                dayArray.append(dayComponents[currentWeekday-1-i])
            }
            for i in currentWeekday...dayComponents.count-1 {
                dayArray.append(dayComponents[dayComponents.count-i+currentWeekday-1])
            }
            dayArray = dayArray.reversed()
            
            let numbers = generateRandomNumber(count: dayArray.count)
            dayLabel.text = "\(pastNum) - \(currentNum) \(monthComponents[currentMonth]) \(currentYear)"
            scoreLabel.text = "\(average(numbers: numbers))"
            barChart(dataEntryX: dayArray, dataEntryY: numbers, xAxisName: dayArray, separator: 1)
            dayArray.removeAll()
            break
            
        case 2:
            let date = getDate(value: -30)
            let pastNum = date["Past"]![0]
            let pastMonth = date["Past"]![1]
            let currentNum = date["Now"]![0]
            let currentMonth = date["Now"]![1]-1
            let currentYear = date["Now"]![2]
            
            var collectCurrentMonth: [String] = []
            var collectLastMonth: [String] = []
            
            for i in 1...currentNum {
                collectCurrentMonth.append(String(i))
            }
            for i in pastNum...pastNum+(30-currentNum) {
                collectLastMonth.append(String(i))
            }
            dayArray = collectLastMonth + collectCurrentMonth
            
            let numbers = generateRandomNumber(count: dayArray.count)
            dayLabel.text = "\(pastNum) \(monthComponents[pastMonth]) - \(currentNum) \(monthComponents[currentMonth]) \(currentYear)"
            scoreLabel.text = "\(average(numbers: numbers))"
            barChart(dataEntryX: dayArray, dataEntryY: numbers, xAxisName: dayArray, separator: 7)
            dayArray.removeAll()
            break
            
        case 3:
            let date = getDate(value: -365)
            let pastMonth = date["Past"]![1]-1
            let pastYear = date["Past"]![2]
            let currentMonth = date["Now"]![1]-1
            let currentYear = date["Now"]![2]
            
            for i in 0...currentMonth {
                dayArray.append(monthComponents[currentMonth-i])
            }
            for i in date["Now"]![1]...monthComponents.count-1 {
                dayArray.append(monthComponents[monthComponents.count-i+currentMonth])
            }
            dayArray = dayArray.reversed()
            
            let numbers = generateRandomNumber(count: dayArray.count)
            dayLabel.text = "\(monthComponents[pastMonth]) \(pastYear) - \(monthComponents[currentMonth]) \(currentYear)"
            scoreLabel.text = "\(average(numbers: numbers))"
            barChart(dataEntryX: dayArray, dataEntryY: numbers, xAxisName: dayArray, separator: 1)
            dayArray.removeAll()
            break
            
        default:
            break
        }
    }
}

extension GraphViewController {
    func generateRandomNumber (count: Int) -> [Int] {
        var numbers: [Int] = []
        for _ in 1...count {
            numbers.append(Int.random(in: 40..<100))
        }
        
        return numbers
    }
    
    func average(numbers: [Int]) -> Int {
        var number = 0
        for i in numbers {
            number += i
        }
        
        return number/numbers.count
    }
}
