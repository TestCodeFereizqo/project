//
//  ViewController.swift
//  TestGraph
//
//  Created by Fereizqo Sulaiman on 18/11/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import Charts

class ViewController: UIViewController {

    @IBOutlet weak var timeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var barView: BarChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    // Calculate date
    func getDate(value: Int, formatter: String) -> Dictionary<String,[Int]> {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = formatter

        let calendar = Calendar.current
        let pastCalendar = Calendar.current.date(byAdding: .day, value: value, to: date)
        let pastDate = [calendar.component(.day, from: pastCalendar!),
                        calendar.component(.month, from: pastCalendar!),
                        calendar.component(.year, from: pastCalendar!)]
        
//        let currentDate = [format.string(from: date)]
        let currentDate = [calendar.component(.day, from: date),
                        calendar.component(.month, from: date),
                        calendar.component(.year, from: date),
                        calendar.component(.weekday, from: date)]

        let collectDate = ["Past": pastDate, "Now": currentDate]
        return collectDate
    }
    
    func barChart(dataEntryX entryX:[String], dataEntryY entryY:[Int], xAxisName xName:[String], separator: Double) {
        // Load data
        var dataEntry: [BarChartDataEntry] = []
        for i in 0...entryX.count-1 {
            let entry = BarChartDataEntry(x: Double(i), y: Double(entryY[i]))
            dataEntry.append(entry)
        }
        let dataSet = BarChartDataSet(entries: dataEntry)
        let data = BarChartData(dataSets: [dataSet])
        barView.data = data
        
        dataSet.colors = [UIColor(red: 247/255, green: 196/255, blue: 67/255, alpha: 1)]
        
        barChartAppearance(xAxisName: xName, separator: separator )
        barView.notifyDataSetChanged()
    }
    
    func barChartAppearance(xAxisName: [String], separator: Double) {
        let legend = barView.legend
        legend.enabled = false
            
        let xAxis = barView.xAxis
        xAxis.drawGridLinesEnabled = true
        xAxis.gridColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        xAxis.labelPosition = .bottom
        xAxis.valueFormatter = IndexAxisValueFormatter(values: xAxisName)
        xAxis.granularity = separator
            
        let yLeftAxis = barView.leftAxis
        yLeftAxis.gridColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        yLeftAxis.drawLabelsEnabled = false
            
        let yRightAxis = barView.rightAxis
        yRightAxis.gridColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        barView.borderLineWidth = 1
        barView.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            
        barView.animate(yAxisDuration: 0.2, easingOption: .linear)
    }
    
    @IBAction func tapTimeSegmentedControl(_ sender: UISegmentedControl) {
        let shortMonthComponents = Calendar.current.shortMonthSymbols
        let monthComponents = Calendar.current.shortMonthSymbols
        let dayComponents = Calendar.current.shortWeekdaySymbols
        var dayArray:[String] = []
        switch timeSegmentedControl.selectedSegmentIndex {
        case 0:
            dayLabel.text = "Today"
            for i in 0..<24 {
                if i < 10 {
                    dayArray.append("0\(i)")
                } else {
                    dayArray.append("\(i)")
                }
            }
            barChart(dataEntryX: dayArray, dataEntryY: [7,5,3,4,3,6,7,2,9,5,4,6,7,4,4,6,7,8,9,10,8,6,3,4], xAxisName: dayArray, separator: 7)
            break
            
        case 1:
            let date = getDate(value: -7, formatter: "dd MMM yyyy")
            dayLabel.text = "\(date["Past"]![0]) - \(date["Now"]![0]) \(monthComponents[Int(date["Now"]![1])-1]) \(date["Now"]![2])"
            for i in 0...date["Now"]![3]-1 {
                dayArray.append(dayComponents[date["Now"]![3]-1-i])
            }
            for i in date["Now"]![3]...dayComponents.count-1 {
                dayArray.append(dayComponents[dayComponents.count-i+date["Now"]![3]-1])
            }
            dayArray = dayArray.reversed()
            barChart(dataEntryX: dayArray, dataEntryY: [3,2,1,8,5,2,7], xAxisName: dayArray, separator: 1)
            dayArray.removeAll()
            break
            
        case 2:
            let date = getDate(value: -30, formatter: "dd MMM yyyy")
            dayLabel.text = "\(date["Past"]![0]) \(monthComponents[Int(date["Past"]![1])-1]) - \(date["Now"]![0]) \(monthComponents[Int(date["Now"]![1])-1]) \(date["Now"]![2])"
            var currentMonth: [String] = []
            var lastMonth: [String] = []
            let a = date["Now"]![0]
            let b = 30 - a
            let c = date["Past"]![0]
            for i in 1...a {
                currentMonth.append(String(i))
            }
            for i in c...c+b {
                lastMonth.append(String(i))
            }
            dayArray = lastMonth + currentMonth
            barChart(dataEntryX: dayArray, dataEntryY: [1,2,2,4,1,6,3,8,1,10,1,8,3,4,1,6,7,2,9,10,1,2,7,4,5,6,7,8,3,2,1], xAxisName: dayArray, separator: 7)
            break
            
        case 3:
            let date = getDate(value: -365, formatter: "MMM yyyy")
            dayLabel.text = "\(monthComponents[Int(date["Past"]![1])-1]) \(date["Past"]![2]) - \(monthComponents[Int(date["Now"]![1])-1]) \(date["Now"]![2])"
            for i in 0...date["Now"]![1]-1 {
                dayArray.append(shortMonthComponents[date["Now"]![1]-1-i])
            }
            for i in date["Now"]![1]...shortMonthComponents.count-1 {
                dayArray.append(shortMonthComponents[shortMonthComponents.count-i+date["Now"]![1]-1])
            }
            dayArray = dayArray.reversed()
            barChart(dataEntryX: dayArray, dataEntryY: [14,2,7,4,5,3,7,8,1,6,10,12], xAxisName: dayArray, separator: 1)
            dayArray.removeAll()
            break
            
        default:
            break
        }
        
    }
}

