//
//  ViewController.swift
//  TemperatureControl
//
//  Created by Fereizqo Sulaiman on 09/04/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var emoji: UILabel!
    @IBOutlet weak var suhu: UILabel!
    @IBOutlet weak var derajat: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeEmoji()
        // Do any additional setup after loading the view.
    }
    

    @IBAction func sliderChanged(_ sender: Any) {
        changeEmoji()

    }
    
    func changeEmoji(){
        switch slider.value {
        case 10:
            emoji.text = ("🇮🇸❄️")
            self.view.backgroundColor = #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1)
        case 11...40:
            emoji.text = ("🥶🙂")
            self.view.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        case 28:
            emoji.text = ("👌🏼☀️")
            self.view.backgroundColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
        case 30...44:
            emoji.text = ("🥵🔥")
            self.view.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        case 46...50:
            emoji.text = ("💀")
            self.view.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        default:
            print ("It's something else")
        }
        let suhuInt:Int = Int(slider.value)
        let suhuStr = String(suhuInt)
        suhu.text = suhuStr
    }
    
    
}

