//
//  GameViewController.swift
//  Trial
//
//  Created by Fereizqo Sulaiman on 05/06/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {

    @IBOutlet weak var playButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func tappedPlayButton(_ sender: Any) {
//        NotificationCenter.default.addObserver(self, selector: #selector(goToDifferentView), name: "back" as? NSNotification.Name, object: nil)
        playButton.isHidden = true
        let scene = GameScene(size: view.bounds.size)
//        let scene = GameOverScene(size: view.bounds.size)
        let skView = view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .resizeFill
        scene.viewController = self
        skView.presentScene(scene)
    }
    
    @objc func goToDifferentView() {
        self.performSegue(withIdentifier: "back", sender: self)
    }
}
