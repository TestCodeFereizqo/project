//
//  GameOverViewController.swift
//  Trial
//
//  Created by Fereizqo Sulaiman on 13/06/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import SpriteKit

class GameOverViewController: UIViewController {

    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    @IBOutlet weak var highScoreLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let score = UserDefaults.standard.integer(forKey: "scoreHandDestroyed")
        if score > UserDefaults.standard.integer(forKey: "highScore") {
            UserDefaults.standard.set(score, forKey: "highScore")
        }
        let highScore = UserDefaults.standard.integer(forKey: "highScore")
        scoreLabel.text = "Your score is \(score)"
        highScoreLabel.text = "Highscore \(highScore)"
        
    }

}
