//
//  GameScene.swift
//  Trial
//
//  Created by Fereizqo Sulaiman on 05/06/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var viewController: UIViewController?
    
    struct PhysicsCategory {
        static let none         : UInt32 = 0
        static let all          : UInt32 = UInt32.max
        static let hand         : UInt32 = 0b1
        static let projectile   : UInt32 = 0b10
        static let player       : UInt32 = 0b100
    }
    
    let labelScore = SKLabelNode(fontNamed: "Futura")
    var scoreHandDestroyed = 0 {
        didSet {
            labelScore.text = "Score: \(scoreHandDestroyed)"
        }
    }
    let labelLife = SKLabelNode(fontNamed: "Futura")
    var life = 3 {
        didSet {
            labelLife.text = "Life: \(life)"
        }
    }
    
    override func didMove(to view: SKView) {
        backgroundColor = SKColor.white
        
        labelScore.text = "Score: \(scoreHandDestroyed)"
        labelScore.fontSize = 20
        labelScore.fontColor = SKColor.gray
        labelScore.position = CGPoint(x: size.width*3/4, y: size.height*0.9)
        addChild(labelScore)
        
        labelLife.text = "Life: \(life)"
        labelLife.fontSize = 20
        labelLife.fontColor = SKColor.gray
        labelLife.position = CGPoint(x: size.width/5, y: size.height*0.9)
        addChild(labelLife)
        
        physicsWorld.gravity = .zero
        physicsWorld.contactDelegate = self
        
        let player = SKSpriteNode(imageNamed: "player")
        player.position = CGPoint(x: size.width/2, y: size.height/2)
        addChild(player)
        
        player.physicsBody = SKPhysicsBody(rectangleOf: player.size)
        player.physicsBody?.isDynamic = true
        player.physicsBody?.categoryBitMask = PhysicsCategory.player
        player.physicsBody?.contactTestBitMask = PhysicsCategory.hand
        player.physicsBody?.collisionBitMask = PhysicsCategory.none
        
        run(SKAction.repeatForever(SKAction.sequence([SKAction.run(addHand), SKAction.wait(forDuration: 1.5
            )])))
    }
    
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    func random(min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
    }
    
    func addPlayer() {

    }
    
    func addHand() {
        let hand = SKSpriteNode(imageNamed: "monster")
        let actualY = random(min: hand.size.height/2, max: size.height - hand.size.height/2)
        let actualX = random(min: hand.size.width/2, max: size.width - hand.size.width/2)
        hand.position = CGPoint(x: actualX, y: actualY)
        
        addChild(hand)
        
        hand.physicsBody = SKPhysicsBody(rectangleOf: hand.size)
        hand.physicsBody?.isDynamic = true
        hand.physicsBody?.categoryBitMask = PhysicsCategory.hand
        hand.physicsBody?.contactTestBitMask = PhysicsCategory.projectile
        hand.physicsBody?.collisionBitMask = PhysicsCategory.none
        
        let actualDuration = random(min: CGFloat(1.0), max: CGFloat(3.0))
        let actionMove = SKAction.move(to: CGPoint(x: size.width/2, y: size.height/2), duration: TimeInterval(actualDuration))
        let actionMoveDone = SKAction.removeFromParent()
        hand.run(SKAction.sequence([actionMove,actionMoveDone]))
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        let touchLocation = touch.location(in: self)
        
        for touche in touches {
            let location = touche.location(in: self)
            let touchNode = atPoint(location)
            
            if touchNode.name == "retryButton" {
                
//                if let gameScene = GameScene(fileNamed: "GameScene") {
//                    let skView = self.view! as SKView
//                    skView.ignoresSiblingOrder = true
//                    gameScene.scaleMode = .aspectFill
//                    gameScene.size = skView.bounds.size
//                    skView.presentScene(gameScene, transition: SKTransition.fade(withDuration: 2.0))
//                }
                
                let gameScene = GameScene(size: size)
                let transition = SKTransition.fade(withDuration: 0.5)
                self.view?.presentScene(gameScene, transition: transition)
                
                print("retry")
                
            } else if touchNode.name == "backHomeButton" {
                print("back")
                returnToMainMenu()
            }
        }
        
        let projectile = SKSpriteNode(imageNamed: "projectile")
        projectile.position = touchLocation
        addChild(projectile)
        
        projectile.physicsBody = SKPhysicsBody(circleOfRadius: projectile.size.width/2)
        projectile.physicsBody?.isDynamic = true
        projectile.physicsBody?.categoryBitMask = PhysicsCategory.projectile
        projectile.physicsBody?.contactTestBitMask = PhysicsCategory.hand
        projectile.physicsBody?.collisionBitMask = PhysicsCategory.none
        
        let actionFadeOut = SKAction.fadeOut(withDuration: 0.5)
        let actionFadeDone = SKAction.removeFromParent()
        projectile.run(SKAction.sequence([actionFadeOut, actionFadeDone]))
    }
    
    func returnToMainMenu() {
        var vc: UIViewController = UIViewController()
        vc = self.view!.window!.rootViewController!
        self.viewController?.performSegue(withIdentifier: "back", sender: vc)
        print("back To Menu")
    }
    func projectileDidCollideWithMonster(projectile: SKSpriteNode, hand: SKSpriteNode){
        //print("Hit!")
        projectile.removeFromParent()
        hand.removeFromParent()
        
        scoreHandDestroyed += 1
    }
    
    func gameOverScene() {
        let scoreLabel = SKLabelNode(fontNamed: "Futura")
        let highScoreLabel = SKLabelNode(fontNamed: "Futura")
        let retryButton = SKSpriteNode(imageNamed: "Reload")
        let backHomeButton = SKSpriteNode(imageNamed: "Home")
        
        let score = UserDefaults.standard.integer(forKey: "scoreHandDestroyed")
        scoreLabel.text = "Sorry, You Lose \(score)"
        scoreLabel.fontSize = 35
        scoreLabel.fontColor = SKColor.black
        scoreLabel.position = CGPoint(x: size.width/2, y: size.height*0.55)
        
        if score > UserDefaults.standard.integer(forKey: "highScore") {
            UserDefaults.standard.set(score, forKey: "highScore")
        }
        let highScore = UserDefaults.standard.integer(forKey: "highScore")
        highScoreLabel.text = "Highscore: \(highScore)"
        highScoreLabel.fontSize = 20
        highScoreLabel.fontColor = SKColor.black
        highScoreLabel.position = CGPoint(x: size.width/2, y: size.height*0.5)
        
        retryButton.position = CGPoint(x: size.width*0.4, y: size.height*0.4)
        retryButton.name = "retryButton"
        
        backHomeButton.position = CGPoint(x: size.width*0.6, y: size.height*0.4)
        backHomeButton.name = "backHomeButton"
        
        addChild(scoreLabel)
        addChild(highScoreLabel)
        addChild(retryButton)
        addChild(backHomeButton)
    }
}

extension GameScene: SKPhysicsContactDelegate {
    func didBegin(_ contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        if ((firstBody.categoryBitMask & PhysicsCategory.hand != 0) && (secondBody.categoryBitMask & PhysicsCategory.projectile != 0)) {
            if let hand = firstBody.node as? SKSpriteNode, let projectile = secondBody.node as? SKSpriteNode {
                projectileDidCollideWithMonster(projectile: projectile, hand: hand)
            }
        } else if ((firstBody.categoryBitMask & PhysicsCategory.hand != 0) && (secondBody.categoryBitMask & PhysicsCategory.player != 0)) {
            life -= 1
            if life == 0 {
//                let scene = GameOverScene(size: self.size)
//                let transition: SKTransition = SKTransition.fade(withDuration: 0.5)
//                UserDefaults.standard.set(scoreHandDestroyed, forKey: "scoreHandDestroyed")
//                self.view?.presentScene(scene, transition: transition)
                removeAllActions()
                removeAllChildren()
                gameOverScene()
            }
        }
    }
    
}
