//
//  GameOverScene.swift
//  Trial
//
//  Created by Fereizqo Sulaiman on 10/06/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import SpriteKit

class GameOverScene: SKScene {
    
    var viewController: UIViewController?
    
    let scoreLabel = SKLabelNode(fontNamed: "Futura")
    let highScoreLabel = SKLabelNode(fontNamed: "Futura")
    let retryButton = SKSpriteNode(imageNamed: "Reload")
    let backHomeButton = SKSpriteNode(imageNamed: "Home")
    
    override func didMove(to view: SKView) {
        backgroundColor = SKColor.white
        let score = UserDefaults.standard.integer(forKey: "scoreHandDestroyed")
        scoreLabel.text = "Sorry, You Lose \(score)"
        scoreLabel.fontSize = 35
        scoreLabel.fontColor = SKColor.black
        scoreLabel.position = CGPoint(x: size.width/2, y: size.height*0.55)
        
        if score > UserDefaults.standard.integer(forKey: "highScore") {
            UserDefaults.standard.set(score, forKey: "highScore")
        }
        let highScore = UserDefaults.standard.integer(forKey: "highScore")
        highScoreLabel.text = "Highscore: \(highScore)"
        highScoreLabel.fontSize = 20
        highScoreLabel.fontColor = SKColor.black
        highScoreLabel.position = CGPoint(x: size.width/2, y: size.height*0.5)
        
        retryButton.position = CGPoint(x: size.width*0.4, y: size.height*0.4)
        retryButton.name = "retryButton"
        
        backHomeButton.position = CGPoint(x: size.width*0.6, y: size.height*0.4)
        backHomeButton.name = "backHomeButton"
        
        addChild(scoreLabel)
        addChild(highScoreLabel)
        addChild(retryButton)
        addChild(backHomeButton)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)
            let touchNode = atPoint(location)
            if touchNode.name == "retryButton" {
                print("retry")
                let gameScene = GameScene(size: size)
                let transition = SKTransition.fade(withDuration: 0.5)
                self.view?.presentScene(gameScene, transition: transition)
            } else if touchNode.name == "backHomeButton" {
//                NotificationCenter.default.post(name: ("back" as? NSNotification.Name)!, object: nil)
                
                returnToMainMenu()
                
//                self.view?.window?.rootViewController?.performSegue(withIdentifier: "back", sender: self)
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let vc = storyboard.instantiateViewController(withIdentifier: "Home")
                
//                let gameScene = GameScene(size: size)
//                let transition = SKTransition.fade(withDuration: 0.5)
//                self.view?.presentScene(gameScene, transition: transition)
                
            }
        }
    }
    
    func returnToMainMenu() {
        var vc: UIViewController = UIViewController()
        vc = self.view!.window!.rootViewController!
//        vc.performSegue(withIdentifier: "back", sender: self)
        self.viewController?.performSegue(withIdentifier: "back", sender: vc)
        print("back To Menu")
    }
}
