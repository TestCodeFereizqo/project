//
//  ViewController.swift
//  TableViewExample
//
//  Created by Fereizqo Sulaiman on 23/04/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

//2. Conform to UITableViewDelegate & UITableViewDataSource (protocols)
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //1.Initializa tableview
    @IBOutlet weak var tableView: UITableView!
    
    // Create tableview data
    let fruits = ["Apple","Orange","Guava","Grapes"]
    let countries = ["Indonesia","USA"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //3. Set tableview datasource
        tableView.delegate = self
        tableView.dataSource = self
        
        //4. Register cell that you want to use
        // CellReuseIdentifier can be random string
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cellTable")
    }
    
    //5. Implement tableview's data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return fruits.count
        } else if  section == 1{
            return countries.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Create / reuse cell from tableview
        // Identifiew must be the same with one we regitered
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellTable", for: indexPath)
        
        //6. Configure the cell
        print("this is row \(indexPath.row)")
        // Section 0 fruits
        // Section 1 countries
        if indexPath.section == 0 {
            cell.textLabel?.text = fruits[indexPath.row]
        } else {
            cell.textLabel?.text = countries[indexPath.row]
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    // Implement tableview delegate
    // didSelectRowAt -> Cell / Row is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Cell is tapped at section \(indexPath.section) and \(indexPath.row)")
    }
}

