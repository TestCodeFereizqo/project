//
//  ViewController.swift
//  MemeMaker
//
//  Created by Fereizqo Sulaiman on 18/05/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var topCaptionSegmentedControl: UISegmentedControl!
    @IBOutlet weak var bottonCaptionSegmentedControl: UISegmentedControl!
    @IBOutlet weak var topCaptionLabel: UILabel!
    @IBOutlet weak var bottomCaptionLabel: UILabel!
    
    var topChoices = [CaptionOption] ()
    let firstChoice = CaptionOption(emoji: "🤖", caption: "You know what's cool?")
    let secondChoice = CaptionOption(emoji: "🐮", caption: "You know what makes me mad?")
    let thirdChoice = CaptionOption(emoji: "🏘", caption: "You know what i love?")
    
    var bottomChoices = [CaptionOption] ()
    let catChoice = CaptionOption(emoji: "🐱", caption: "Cat wearing hats")
    let dogChoice = CaptionOption(emoji: "🐕", caption: "Dogs carrying logs")
    let monkeyChoice = CaptionOption(emoji: "🐒", caption: "Monkeys being funky")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        topChoices = [firstChoice,secondChoice,thirdChoice]
        bottomChoices = [catChoice,dogChoice,monkeyChoice]
        
        topCaptionSegmentedControl.removeAllSegments()
        for choice in topChoices {
            topCaptionSegmentedControl.insertSegment(withTitle: choice.emoji, at: topChoices.count, animated: false)
        }
        topCaptionSegmentedControl.selectedSegmentIndex = 0
        
        bottonCaptionSegmentedControl.removeAllSegments()
        for choice in bottomChoices {
            bottonCaptionSegmentedControl.insertSegment(withTitle: choice.emoji, at: bottomChoices.count, animated: false)
        }
        bottonCaptionSegmentedControl.selectedSegmentIndex = 0
        
        updateLabel()
    }
    
    func updateLabel() {
        let topIndex = topCaptionSegmentedControl.selectedSegmentIndex
        let bottomIndex = bottonCaptionSegmentedControl.selectedSegmentIndex
        
        let topChoice = topChoices[topIndex]
        let bottomChoice = bottomChoices[bottomIndex]
        
        topCaptionLabel.text = topChoice.caption
        bottomCaptionLabel.text = bottomChoice.caption
    }
    
    @IBAction func topTappedSegmentedControl(_ sender: UISegmentedControl) {
        updateLabel()
    }
    
    @IBAction func bottomTappedSegmentedControl(_ sender: UISegmentedControl) {
        updateLabel()
    }
    
}

