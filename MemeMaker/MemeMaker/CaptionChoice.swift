//
//  CaptionChoice.swift
//  MemeMaker
//
//  Created by Fereizqo Sulaiman on 18/05/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import Foundation

struct CaptionOption {
    var emoji: String
    var caption: String
}
