//
//  Brain.swift
//  FizzBuzz
//
//  Created by Fereizqo Sulaiman on 05/02/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Foundation

class Brain {
    
    func isDivisibleBy(number: Int, divider: Int) -> Bool {
         if number % divider == 0 {
             return true
         } else {
             return false
         }
     }
    
    func isDivisibleByThree(number: Int) -> Bool {
        isDivisibleBy(number: number, divider: 3)
    }

    func isDivisibleByFive(number: Int) -> Bool {
        isDivisibleBy(number: number, divider: 5)
    }

    func isDivisibleByFifteen(number: Int) -> Bool {
        isDivisibleBy(number: number, divider: 15)
    }
    
    func check(number: Int) -> String {
        if isDivisibleByThree(number: number) {
            return "Fizz"
        } else if isDivisibleByFive(number: number) {
            return "Buzz"
        } else if isDivisibleByFifteen(number: number) {
            return "FizzBuzz"
        } else {
            return "\(number)"
        }
    }
    
 
}
