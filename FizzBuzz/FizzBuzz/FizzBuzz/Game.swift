//
//  Game.swift
//  FizzBuzz
//
//  Created by Fereizqo Sulaiman on 05/02/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Foundation

class Game {
//    var score = 0
    
    var score: Int?
    var brain: Brain
    
    init() {
        score = 0
        brain = Brain()
    }
    
    func play(move: String) -> (right: Bool, score: Int) {
        let result = brain.check(number: score! + 1)
            
        if result == move {
            score! += 1
            return (true, score!)
        } else {
            return (false, score!)
        }
    }
    
}
