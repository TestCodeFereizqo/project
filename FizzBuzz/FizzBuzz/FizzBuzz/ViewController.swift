//
//  ViewController.swift
//  FizzBuzz
//
//  Created by Fereizqo Sulaiman on 05/02/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var game: Game?
    var gameScore: Int? {
        didSet {
            guard let unwrappedScore = gameScore else {
                print("gameScore is nil")
                return
            }
            
            numberButton.setTitle("\(unwrappedScore)", for: .normal)
        }
    }
    
    @IBOutlet weak var numberButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        game = Game()
        
        guard let checkedGame = game else {
            print("game is nil")
            return
        }
        
        gameScore = checkedGame.score
        
    }
    
    func play(move: String) {
        guard let unwrappedGame = game else {
            print("The unwrappedGame is nil")
            return
        }
        
        let response = unwrappedGame.play(move: move)
        gameScore = response.score
        
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        guard let unwrappedScore = gameScore else {
            print("The score is nil")
            return
        }
        let nextScore = unwrappedScore + 1
        
        play(move: "\(nextScore)")
    }
}

