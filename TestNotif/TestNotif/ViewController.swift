//
//  ViewController.swift
//  TestNotif
//
//  Created by Fereizqo Sulaiman on 08/12/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    @IBOutlet weak var sendNotifButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        UNUserNotificationCenter.current().requestAuthorization(options: (.alert), completionHandler: {(granted, error) in
            if granted {
                print("yes")
            } else {
                print("no")
            }
        })
    }


    @IBAction func tapSendNotifButton(_ sender: UIButton) {
        print("tap")
        let content = UNMutableNotificationContent()
        content.title = "Testing Notification"
        content.subtitle = "testing testint testing"
        content.body = "Notication triggered"
        
//        let imageName = "applelogo"
//        guard let imageURL = Bundle.main.url(forResource: imageName, withExtension: "png") else { return }
//        let attachment = try! UNNotificationAttachment(identifier: imageName, url: imageURL, options: .none)
//        content.attachments = [attachment]
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        let request = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}

