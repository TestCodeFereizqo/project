//
//  ViewController.swift
//  TestingLanguage
//
//  Created by Fereizqo Sulaiman on 23/10/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var byeLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let alertTitle = NSLocalizedString("Welcome", comment: "")
        let alertMessage = NSLocalizedString("Thank you", comment: "")
        let cancelButtonText = NSLocalizedString("Cancel", comment: "")
//        let signUpButtobText = NSLocalizedString("Signup", comment: "")
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertController.Style.alert)
        let cancelAction = UIAlertAction(title: cancelButtonText, style: UIAlertAction.Style.cancel, handler: nil)
//        let signUpAction = UIAlertAction(title: signUpButtobText, style: UIAlertAction.Style.cancel, handler: nil)
        
        alert.addAction(cancelAction)
//        alert.addAction(signUpAction)
        self.present(alert, animated: true, completion: nil)
        
//        welcomeLabel.text = NSLocalizedString("Welcome", comment: "")
//        byeLabel.text = NSLocalizedString("Thank you", comment: "")
    }
    
}

