//
//  ViewController.swift
//  TestLocationGeoFence
//
//  Created by Fereizqo Sulaiman on 03/12/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol HandleMapSearch {
    func dropPinZoomIn(placemark: MKPlacemark)
}

struct PreferencesKeys {
  static let savedItems = "savedItems"
}

class ViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var radiusSlider: UISlider!
    @IBOutlet weak var radiusLabel: UILabel!
    @IBOutlet weak var geotificationTableView: UITableView!
    @IBOutlet weak var confirmButton: UIButton!
    
    var radius: Double = 500
    var resultSearchController: UISearchController? = nil
    var selectedPin: MKPlacemark? = nil
    var geotifications: [Geotifications] = []
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        geotificationTableView.delegate = self
        geotificationTableView.dataSource = self
        loadAllGeotifications()
        
        // Appearances
        radiusSlider.value = Float(radius)
        radiusSlider.isEnabled = false
        radiusSlider.isHidden = true
        confirmButton.isEnabled = false
        confirmButton.isHidden = true
        confirmButton.layer.cornerRadius = 5
        
        // Setup search result table
        let locationSearchTable = storyboard?.instantiateViewController(identifier: "LocationSeachTableViewController") as! LocationSeachTableViewController
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController?.searchResultsUpdater = locationSearchTable
        locationSearchTable.mapView = mapView
        locationSearchTable.handleMapSearchDelegate = self
        
        // Setup search bar
        let searchBar = resultSearchController?.searchBar
        searchBar?.sizeToFit()
        searchBar?.placeholder = "Search for place"
        navigationItem.titleView = resultSearchController?.searchBar
        
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        definesPresentationContext = true
        
        
        // Gesture recognize
        let longPress: UILongPressGestureRecognizer = UILongPressGestureRecognizer()
        longPress.addTarget(self, action: #selector(addAnnotationOnLongPress(gesture:)))
        longPress.minimumPressDuration = 0.5
        mapView.addGestureRecognizer(longPress)
        
        // Location manager
        if CLLocationManager.locationServicesEnabled() {
            mapView.delegate = self
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.distanceFilter = 10
            locationManager.allowsBackgroundLocationUpdates = true
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
        }
    }
    
    func loadAllGeotifications() {
        geotifications = Geotifications.getGeotifications()
        print("There are geotif: \(geotifications.count)")
        if geotifications.count > 0 {
            for model in geotifications {
                makeRadiusCircle(location: CLLocation(latitude: Double(model.latitude)!, longitude: Double(model.longitude)!), size: Double(model.radius)!)
                setupGeofence(location: CLLocation(latitude: Double(model.latitude)!, longitude: Double(model.longitude)!), size: Double(model.radius)!, identifier: model.identifier)
            }
        }
        geotificationTableView.reloadData()
    }
    
    @IBAction func tapConfirmButton(_ sender: UIButton) {
        mapView.removeAnnotations(mapView.annotations)
        mapView.removeOverlays(mapView.overlays)
        
        let identifier = NSUUID().uuidString
        print("identifier: \(identifier)")
        
        let model = Geotifications(radius: "\(radius)",
            latitude: "\(selectedPin?.coordinate.latitude ?? 0)",
            longitude: "\(selectedPin?.coordinate.longitude ?? 0)",
            identifier: identifier,
            address: "\(selectedPin?.thoroughfare ?? "Location address") \(selectedPin?.subThoroughfare ?? "additional address")",
            name: "\(selectedPin?.name ?? "Location name")")
        
        geotifications.append(model)
        Geotifications.setGeotifications(data: geotifications)
        
        setupGeofence(location: (selectedPin?.location)!, size: radius, identifier: identifier)
        print("Geofence with radius: \(radius)")
    }
    
    @IBAction func changeRadiusSlider(_ sender: UISlider) {
        radius = Double(radiusSlider.value)
        radiusLabel.text = "\(radius) meter"
        self.mapView.removeOverlays(self.mapView.overlays)
        makeRadiusCircle(location: (selectedPin?.location!)!, size: radius)
    }
    
    @objc
    func addAnnotationOnLongPress(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .ended {
            let point = gesture.location(in: self.mapView)
            let coordinate = self.mapView.convert(point, toCoordinateFrom: self.mapView)
            print("Long press coordinate: \(coordinate)")
            
            mapView.removeAnnotations(mapView.annotations)
            let geoCoder = CLGeocoder()
            let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            
            geoCoder.reverseGeocodeLocation(location) { (placemarks, error) in
                if error == nil {
                    let placemark = MKPlacemark(placemark: placemarks![0])
                    self.dropPinZoomIn(placemark: placemark)
                } else {
                    print("Reverse geocode fail: \(error?.localizedDescription ?? "Error could not reverse geocode")")
                }
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKCircle {
            let circle = MKCircleRenderer(overlay: overlay)
            circle.strokeColor = .red
            circle.fillColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.1)
            circle.lineWidth = 1
            return circle
        } else {
            return MKOverlayRenderer()
        }
    }
}

extension ViewController {
    func makeRadiusCircle(location: CLLocation, size: Double) {
        let circle = MKCircle(center: location.coordinate, radius: size)
        self.mapView.addOverlay(circle)
    }
    
//    func stopMonitoring(geotification: Geotification) {
//      for region in locationManager.monitoredRegions {
//        guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier == geotification.identifier else { continue }
//        locationManager.stopMonitoring(for: circularRegion)
//      }
//    }
    
    
    // To add geotification
    func setupGeofence(location: CLLocation, size: Double, identifier: String) {
        let geofenceRegion = CLCircularRegion(center: location.coordinate, radius: size, identifier: identifier)
        geofenceRegion.notifyOnEntry = true
        
        self.locationManager.startMonitoring(for: geofenceRegion)
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        print("Latitude: \(locations.last?.coordinate.latitude)")
//        print("Longitude: \(locations.last?.coordinate.longitude)")
//        if let location = locations.first {
//            print("Location: \(location)")
//        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
//        print("You are inside your region")
    }
}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let myPinIdentifier = "PinAnnotationIdentifier"
        
        // Generate pin
        let myPinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: myPinIdentifier)
        
        myPinView.animatesDrop = true
//        myPinView.canShowCallout = true
//        myPinView.image = UIImage(named: "Annotation")
        myPinView.annotation = annotation
        
        return myPinView
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return geotifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = geotifications[indexPath.row].name
        cell.detailTextLabel?.text = geotifications[indexPath.row].address
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let confirmation = UIAlertController(title: "Confirmation", message: "Are your sure want to delete?", preferredStyle: .actionSheet)
            let yesAction = UIAlertAction(title: "Yes", style: .destructive) { (action: UIAlertAction) in
                self.geotifications.remove(at: indexPath.row)
                self.geotificationTableView.deleteRows(at: [indexPath], with: .fade)
                Geotifications.setGeotifications(data: self.geotifications)
                self.mapView.removeOverlay(self.mapView.overlays[indexPath.row])
            }
            let noAction = UIAlertAction(title: "No", style: .default, handler: nil)
            let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
            
            confirmation.addAction(yesAction)
            confirmation.addAction(noAction)
            confirmation.addAction(dismissAction)
            
            present(confirmation, animated: true, completion:nil)
        }
    }
    
}

extension ViewController: HandleMapSearch {
    func dropPinZoomIn(placemark: MKPlacemark) {
        // cache the pin
        selectedPin = placemark
        // clear existing pin
        mapView.removeAnnotations(mapView.annotations)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = placemark.coordinate
        annotation.title = placemark.name
        if let city = placemark.locality,
            let state = placemark.administrativeArea {
            annotation.subtitle = "\(city) \(state)"
        }
        mapView.addAnnotation(annotation)
        
        let center = CLLocationCoordinate2D(latitude: (placemark.coordinate.latitude+(locationManager.location?.coordinate.latitude)!)/2, longitude: (placemark.coordinate.longitude+(locationManager.location?.coordinate.longitude)!)/2)
        
        let region = MKCoordinateRegion(center: center, latitudinalMeters: (placemark.location?.distance(from: locationManager.location!))!*1.5, longitudinalMeters: (placemark.location?.distance(from: locationManager.location!))!*1.5)
        
        mapView.setRegion(region, animated: true)
        radiusSlider.isEnabled = true
        radiusSlider.isHidden = false
        confirmButton.isEnabled = true
        confirmButton.isHidden = false
    }
}


