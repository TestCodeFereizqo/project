//
//  Notifications.swift
//  TestLocationGeoFence
//
//  Created by Fereizqo Sulaiman on 09/12/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import Foundation
import UserNotifications

class Notifications: NSObject, UNUserNotificationCenterDelegate {
    
    let userNotificationCenter = UNUserNotificationCenter.current()
    let notificationContent = UNMutableNotificationContent()
    
    // Request authorization
    func requestNotificationAuthorization() {
        // Authenticate
        let authOptions = UNAuthorizationOptions.init(arrayLiteral: .alert, .badge, .sound)
        
        self.userNotificationCenter.requestAuthorization(options: authOptions) { (success, error) in
            if success {
                print("Local notif: Enable")
            } else if !success {
                print("Local notif: Disable")
            }
            if let error = error {
                print("Error: ", error)
            }
        }
    }
    
    // Make notif content
    func sendNotification() {
        notificationContent.title = "Reminder"
        notificationContent.body = "You have arrived at your location"
        notificationContent.badge = NSNumber(value: 1)
        notificationContent.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "alarm3.aiff"))
        
        // Add an attachment to notif
//        if let url = Bundle.main.url(forResource: "test", withExtension: "mp3") {
//            if let attachment = try? UNNotificationAttachment(identifier: "test", url: url, options: nil) {
//                notificationContent.attachments = [attachment]
//            }
//        }
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: "testNotification", content: notificationContent, trigger: trigger)
        
        userNotificationCenter.add(request) { (error) in
            if let error = error {
                print("Notfications error: ", error)
            }
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
}
