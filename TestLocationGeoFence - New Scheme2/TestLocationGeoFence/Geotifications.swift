//
//  Geotifications.swift
//  TestLocationGeoFence
//
//  Created by Fereizqo Sulaiman on 09/12/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import Foundation
import MapKit

struct Geotifications {
    var radius: String
    var latitude: String
    var longitude: String
    var identifier: String
    var address: String
    var name: String
    
    static let KEY = "GEOTIFICATIONS"
    
    init(radius: String, latitude: String, longitude: String, identifier: String, address: String, name: String) {
        self.radius = radius
        self.latitude = latitude
        self.longitude = longitude
        self.identifier = identifier
        self.address = address
        self.name = name
    }
    
    static func setGeotifications(data: [Geotifications]) {
        var models: [String] = []
        
        for model in data {
            let str = "\(model.radius);\(model.latitude);\(model.longitude);\(model.identifier);\(model.address);\(model.name)"
            
            models.append(str)
        }
        
        UserDefaults.standard.set(models, forKey: KEY)
    }
    
    static func getGeotifications() -> [Geotifications] {
        let models: [String] = UserDefaults.standard.object(forKey: KEY) as? [String] ?? []
        var data: [Geotifications] = []
        
        for model in models {
            let modelArray = model.components(separatedBy: ";")
            print(modelArray)
            let geotification = Geotifications(radius: modelArray[0],
                                               latitude: modelArray[1],
                                               longitude: modelArray[2],
                                               identifier: modelArray[3],
                                               address: modelArray[4],
                                               name: modelArray[5])
            data.append(geotification)
        }
        return data
    }
    
    static func deleteRecentSearchLocation() {
        UserDefaults.standard.removeObject(forKey: KEY)
    }
}

