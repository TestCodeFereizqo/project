//
//  AppDelegate.swift
//  TrialSpriteKit-NinjaAttack
//
//  Created by Fereizqo Sulaiman on 29/05/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

}

