//
//  ViewController.swift
//  ComparatorDogCat
//
//  Created by Fereizqo Sulaiman on 11/11/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import CoreML
import Vision

class ViewController: UIViewController {

    @IBOutlet weak var petImageView: UIImageView!
    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var chooseButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
    }

    @IBAction func tapChooseButton(_ sender: Any) {
        let pickerControl = UIImagePickerController()
        pickerControl.delegate = self
        pickerControl.sourceType = .savedPhotosAlbum
        
        present(pickerControl, animated: true, completion: nil)
    }
    
}

extension ViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            petImageView.image = pickedImage
            guard let ciImage = CIImage(image: pickedImage) else {
              fatalError("couldn't convert UIImage to CIImage")
            }
            detectScene(image: ciImage)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension ViewController: UINavigationControllerDelegate {
    
}

extension ViewController {
    func detectScene(image: CIImage) {
        answerLabel.text = "Detecting scene.."
        
        // Load ML model
        guard let model = try? VNCoreMLModel(for: CatDogClassifier().model) else {
            fatalError("Can't load Cat Dog ML Classifier")
        }
        
        
        // Create vision request
        let request = VNCoreMLRequest(model: model) { [weak self] request, error in
            guard let results = request.results as? [VNClassificationObservation],
                let topResult = results.first else {
                    fatalError("Unexpected result type from VNClassificationObvservation")
            }
            
            //Update UI
            DispatchQueue.main.async {
                self?.answerLabel.text = "\(Int(topResult.confidence * 100))% it's \(topResult.identifier)"
            }
        }
        
        // Run CoreML
        let handler = VNImageRequestHandler(ciImage: image)
        DispatchQueue.global(qos: .userInteractive).async {
            do {
                try handler.perform([request])
            } catch {
                print(error)
            }
        }
        
        //Other Was to Load and Train
//        let modelA = CatDogClassifier()
//        let predict = modelA.prediction(image: <#T##CVPixelBuffer#>)
//        predict.classLabel
        
    }
}
