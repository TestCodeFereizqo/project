//
//  ViewController.swift
//  CheckerBoard
//
//  Created by Fereizqo Sulaiman on 10/04/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var sliderNilai: UISlider!
    @IBOutlet weak var labelNilai: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //makeBox()
        makeSquareBox()
//        for i in 0...5{
//            for j in 0...5{
//                print ("\(i)\(j) ", terminator:"")
//            }
//            print("")
//        }
    }
    @IBAction func sliderChanged(_ sender: UISlider) {
        let nilaiSlider = Int(sender.value)
        labelNilai.text = nilaiSlider.description
        for viewx in view.subviews {
            viewx.removeFromSuperview()
        }
        makeSquareBox()
    }
    func makeBox() {
        let box = UIView()
        box.frame = CGRect(x: 100, y: 100, width: 50, height: 50)
        box.backgroundColor = .black
        view.addSubview(box)
    }
    func makeSquareBox() {
        var coorx = 50
        var coory = 50
        var warna = true
        let size = Int(sliderNilai.value)
        for _ in 1...size {
            for _ in 1...size {
                let box = UIView()
                box.frame = CGRect(x: coorx, y: coory, width: 50, height: 50)
                coorx += 50
                if warna {
                    warna = false
                    box.backgroundColor = .black
                } else {
                    warna = true
                    box.backgroundColor = .white
                }
                view.addSubview(box)
            }
            if size % 2 == 0 {
                warna = !warna
            }
            coorx = 50
            coory += 50
        }
    }
}
