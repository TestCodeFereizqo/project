//
//  Pet.swift
//  TestDI
//
//  Created by Fereizqo Sulaiman on 06/04/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Foundation

// Dependency Injection

/// Without
//class Cat {
//    let name: String
//
//    init(name: String) {
//        self.name = name
//    }
//
//    func Sound() -> String {
//        return "Meow!"
//    }
//}
//
//class PetOwner {
//    let pet = Cat(name: "Meo")
//
//    func play() -> String {
//        return "I'm playing with \(pet.name). \(pet.Sound()) "
//    }
//}
//
//let adi = PetOwner()
//adi.play()

/// With
protocol AnimalType {
    var name: String { get }
    func Sound() -> String
}

class Cat: AnimalType {
    var name: String

    init(name: String) {
        self.name = name
    }

    func Sound() -> String {
        return "Meow!"
    }
}
//
//class Dog: AnimalType {
//    var name: String
//
//    init(name: String) {
//        self.name = name
//    }
//
//    func Sound() -> String {
//        return "Bow!"
//    }
//
//
//}
//
class PetOwner {
    let pet: AnimalType

    init(pet: AnimalType) {
        self.pet = pet
    }

    func play() -> String {
        return "I'm playing with \(pet.name). \(pet.Sound())"
    }
}

//let adi = PetOwner(pet: Cat(name: "Mini"))
//adi.play()
//let ida = PetOwner(pet: Dog(name: "Mono"))
//ida.play()
