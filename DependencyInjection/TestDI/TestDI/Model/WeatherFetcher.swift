//
//  WeatherFetcher.swift
//  TestDI
//
//  Created by Fereizqo Sulaiman on 06/04/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

struct WeatherFetcher {
    
    static func fetch(completion: @escaping ([City]?) -> Void ) {
        Alamofire.request(OpenWeatherMap.url, method: .get, parameters: OpenWeatherMap.parameters)
            .response { result in
                let cities = result.data.map { decode(data: ($0) ) }
                completion(cities)
        }
    }
    
    private static func decode (data: Data) -> [City] {
        var cities: [City] = []
        do {
            let json = try JSON(data: data)
            for (_, j) in json["list"] {
                if let id = j["id"].int {
                    let city = City(
                        id: id,
                        name: j["name"].string ?? "",
                        weather: j["weather"].string ?? "")
                    cities.append(city)
                }
            }
        } catch  {
            print("There is an error when decode")
        }
        
        return cities
    }
}
