//
//  ViewController.swift
//  Weather
//
//  Created by Fereizqo Sulaiman on 10/04/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    
    var cities: [City]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        fetch{ self.cities = $0 }
    }
    
    func fetch(response: @escaping([City]?) -> Void) {
        Alamofire.request(OpenWeatherMap.url, method: .get, parameters: OpenWeatherMap.parameters)
            .response { result in
                let cities = result.data.map { self.decode(data: $0) }
                response(cities)
        }
        
    }
    
    func decode(data: Data) -> [City] {
        var cities = [City]()
        
        do {
            let json = try JSON(data: data)
            for (_,j) in json["list"] {
                if let id = j["id"].int {
                    let city = City(
                        id: id,
                        name: j["name"].string ?? "",
                        weather: j["weather"].string ?? "")
                    cities.append(city)
                }
            }
        } catch {
            print("There is an error")
        }
        
        
        return cities
    }

}

