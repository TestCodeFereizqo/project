//
//  WeatherFetcher.swift
//  Weather
//
//  Created by Fereizqo Sulaiman on 10/04/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Foundation
import SwiftyJSON

struct WeatherFetcher {
    let networking: Networking
    
    func fetch(response: @escaping([City]?) -> Void) {
        networking.request { result in
            let cities = self.decode(data: result)
            
            response(cities)
        }
    }
    
    private func decode(data: Data) -> [City] {
        var cities = [City]()
        
        do {
            let json = try JSON(data: data)
            for (_,j) in json["list"] {
                if let id = j["id"].int {
                    let city = City(
                        id: id,
                        name: j["name"].string ?? "",
                        weather: j["weather"].string ?? "")
                    cities.append(city)
                }
            }
        } catch {
            print("There is an error")
        }
        
        
        return cities
    }
}
