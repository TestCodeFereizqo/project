//
//  Network.swift
//  Weather
//
//  Created by Fereizqo Sulaiman on 10/04/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Foundation
import Alamofire

struct Network: Networking {
    func request(response: @escaping (Data) -> Void) {
        Alamofire.request(OpenWeatherMap.url, method: .get, parameters: OpenWeatherMap.parameters).response { result in
            guard let data = result.data else { return }
            response(data)
        }
    }
}
