//
//  Networking.swift
//  Weather
//
//  Created by Fereizqo Sulaiman on 10/04/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Foundation

protocol Networking {
    func request (response: @escaping(Data) -> Void)
}
