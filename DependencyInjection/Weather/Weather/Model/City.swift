//
//  City.swift
//  Weather
//
//  Created by Fereizqo Sulaiman on 10/04/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Foundation

struct City {
    let id: Int
    let name: String
    let weather: String
}
