//
//  OpenWeatherMap.swift
//  Weather
//
//  Created by Fereizqo Sulaiman on 10/04/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Foundation

struct OpenWeatherMap {
    private static let apiKey = "ef116ed6e0d00e079d6acc0ae2ad7c75"
    
    private static let cityIds = [
        6077243, 524901, 5368361, 1835848, 3128760, 4180439,
        2147714, 264371, 1816670, 2643743, 3451190, 1850147
    ]
    
    static let url = "https://api.openweathermap.org/data/2.5/group"
    
    static var parameters: [String:String] {
        return [
            "APPID": apiKey,
            "id": cityIds.map { String($0) }.joined(separator: ",")
        ]
    }
}
