//
//  WeatherFetcherSpec.swift
//  WeatherTests
//
//  Created by Fereizqo Sulaiman on 10/04/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import Nimble
import Quick
import Swinject
import Alamofire
@testable import Weather

class WeatherFetcherSpec: QuickSpec {
    
    struct StubNetwork: Networking {
        private static let json =
        "{" +
            "\"list\": [" +
                "{" +
                    "\"id\": 2643743," +
                    "\"name\": \"London\"," +
                    "\"weather\": [" +
                        "{" +
                            "\"main\": \"Rain\"" +
                        "}" +
                    "]" +
                "}," +
                "{" +
                    "\"id\": 3451190," +
                    "\"name\": \"Rio de Janeiro\"," +
                    "\"weather\": [" +
                        "{" +
                            "\"main\": \"Clear\"" +
                        "}" +
                    "]" +
                "}" +
            "]" +
        "}"
        
        func request(response: @escaping (Data) -> Void) {
            guard let data = StubNetwork.json.data(using: String.Encoding.utf8, allowLossyConversion: false) else { return }
            response(data)
        }
    }
    
    override func spec() {
        /// Test Without DI
//        it("return cities") {
//            var cities: [City]?
//            WeatherFetcher.fetch{ cities = $0 }
//
//            expect(cities).toEventuallyNot(beNil())
//            expect(cities?.count).toEventually(equal(12))
//            expect(cities?[0].id).toEventually(equal(6077243))
//            expect(cities?[0].name).toEventually(equal("Montreal"))
//            expect(cities?[0].weather).toEventually(equal("Cloudy"))
//        }
        
        /// Test With DI
        var container: Container!
        beforeEach {
            container = Container()
            
            // Registration for the network using Alamofire
            container.register(Networking.self) { _ in Network()}
            container.register(WeatherFetcher.self) { r in
                WeatherFetcher(networking: r.resolve(Networking.self)!)
            }
            
            // Registration for the stub network
            container.register(Networking.self, name: "stub") { _ in StubNetwork()}
            container.register(WeatherFetcher.self, name: "stub") { r in
                WeatherFetcher(networking: r.resolve(Networking.self, name: "stub")!)
            }
        }
        
        it("return cities") {
            var cities: [City]?
            let fetcher = container.resolve(WeatherFetcher.self)!
            fetcher.fetch { cities = $0 }

            expect(cities).toEventuallyNot(beNil())
            expect(cities?.count).toEventually(beGreaterThan(0))
        }
        
        it("fills the weather data") {
            var cities: [City]?
            let fetcher = container.resolve(WeatherFetcher.self, name: "stub")
            fetcher?.fetch { cities = $0 }
            
            expect(cities?[0].id).toEventually(equal(2643743))
            expect(cities?[0].name).toEventually(equal("London"))
//            expect(cities?[0].weather).toEventually(equal("Rain"))
        }
        
    }
    
}
