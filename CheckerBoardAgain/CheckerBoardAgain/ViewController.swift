//
//  ViewController.swift
//  CheckerBoardAgain
//
//  Created by Fereizqo Sulaiman on 10/04/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var banyakKotakLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //makeBox()
        makePlus()
    }
    @IBAction func sliderChanged(_ sender: UISlider) {
        //makeBox()
        makePlus()
    }
    func makeBox() {
        let banyakKotak = Int(slider.value)
        banyakKotakLabel.text = String(banyakKotak)
        let ukuranKotak = 50
        var posisiX = 50
        var posisiY = 50
        var kotak = true
        for _ in 1...banyakKotak {
            for _ in 1...banyakKotak {
                let box = UIView()
                box.frame = CGRect(x: posisiX, y: posisiY, width: ukuranKotak, height: ukuranKotak)
                box.backgroundColor = .black
                view.addSubview(box)
                posisiX += ukuranKotak
                if kotak{
                    kotak = false
                    box.backgroundColor = .white
                } else {
                    kotak = true
                    box.backgroundColor = .black
                }
            }
            posisiX = 50
            posisiY += ukuranKotak
            if banyakKotak % 2 == 0 {
                kotak = !kotak
            }
        }
        
    }

    
    
    func makePlus() {
        let banyakKotak = 3
        let ukuranKotak = 30
        var posisiX = 30
        var posisiY = 30
        
        for i in 1...(banyakKotak*2-1) {
            for j in 1...(banyakKotak*2-1) {
                let box = UIView()
                box.frame = CGRect(x: posisiX, y: posisiY, width: ukuranKotak, height: ukuranKotak)
                box.backgroundColor = .black
                view.addSubview(box)
                if j == banyakKotak {
                    
                }
            }
            //posisiY += ukuranKotak
        }
    }
}
    
//    func makePlus() {
//        let banyakKotak = 3
//        let ukuranKotak = 30
//        var posisiX = 30
//        var posisiY = 30
//
//       for i in 1...(banyakKotak*2-1) {
//        if i == banyakKotak {
//            for _ in 1...(banyakKotak*2-1) {
//                    let box = UIView()
//                    box.frame = CGRect(x: posisiX, y: posisiY, width: ukuranKotak, height: ukuranKotak)
//                    box.backgroundColor = .black
//                    view.addSubview(box)
//                    posisiX +=  ukuranKotak
//                }
//        } else {
//            posisiX = (banyakKotak-1)*ukuranKotak
//            posisiY += ukuranKotak
//            let box1 = UIView()
//            box1.frame = CGRect(x: posisiX, y: posisiY, width: ukuranKotak, height: ukuranKotak)
//            box1.backgroundColor = .blue
//            view.addSubview(box1)
//            }
//        }
//    }
//}
