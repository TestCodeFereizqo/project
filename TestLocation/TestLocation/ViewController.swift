//
//  ViewController.swift
//  TestLocation
//
//  Created by Fereizqo Sulaiman on 22/11/19.
//  Copyright © 2019 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var doneTripButton: UIButton!
    
    let locationManager = CLLocationManager()
    var startLocation: CLLocation!
    var lastLocation: CLLocation!
    var collectLocation: [CLLocationCoordinate2D] = []
    var startDate: Date!
    var traveledDistance: Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        doneTripButton.layer.cornerRadius = 10
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.distanceFilter = 10
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
            mapView.delegate = self
        }
        
//        doneTripButton.isHidden = true
//        doneTripButton.isEnabled = false
    }
    
    @IBAction func tapDoneTrip(_ sender: UIButton) {
        createTestPolyLine()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations.last ?? "none")
        
        if startDate == nil {
            startDate = Date()
        } else {
            print("elapsedTime:", String(format: "%.0fs", Date().timeIntervalSince(startDate)))
        }
        if startLocation == nil {
            startLocation = locations.first
        } else if let location = locations.last {
            traveledDistance += lastLocation.distance(from: location)
            print("Traveled Distance:",  traveledDistance)
            print("Straight Distance:", startLocation.distance(from: locations.last!))
        }
        lastLocation = locations.last
        collectLocation.append(locations.last!.coordinate)
    }
    
    func createTestPolyLine(){
           let locations = collectLocation
           let aPolyline = MKPolyline(coordinates: locations, count: locations.count)
           
           mapView.addOverlay(aPolyline)
       }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer! {
        if (overlay is MKPolyline) {
            let pr = MKPolylineRenderer(overlay: overlay)
            pr.lineWidth = 5
            pr.strokeColor = UIColor.red.withAlphaComponent(0.5)
            print("This is polyline renderer : \(pr)")
            return pr
        }

        return nil
    }
}

