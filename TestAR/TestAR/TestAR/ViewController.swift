//
//  ViewController.swift
//  TestAR
//
//  Created by Fereizqo Sulaiman on 25/01/20.
//  Copyright © 2020 Fereizqo Sulaiman. All rights reserved.
//

import UIKit
import RealityKit
import ARKit
import Combine

class ViewController: UIViewController, ARSessionDelegate {
    
    @IBOutlet var arView: ARView!
    
    // Character to display
    var character: BodyTrackedEntity?
    let characterAnchor = AnchorEntity()
    let characterOffset: SIMD3<Float> = [-1.0,0,0]
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        arView.session.delegate = self
        
        // Check device support AR
        guard ARBodyTrackingConfiguration.isSupported else {
            fatalError("This feature only supported on devices with an A12 Chip")
        }
        
        // Run Body tracking config
        let configuration = ARBodyTrackingConfiguration()
        arView.session.run(configuration)
        arView.scene.addAnchor(characterAnchor)
        
        // Asyncrhonously Load Character Model
        var cancellable: AnyCancellable? = nil
        /// Load model
        cancellable = Entity.loadBodyTrackedAsync(named: "character/robot").sink(
            receiveCompletion: { completion in
                if case let .failure(error) = completion {
                    print("Error: Unable to load model \(error.localizedDescription)")
                }
                cancellable?.cancel()
        /// Scaling model
        }, receiveValue: { (character: Entity) in
            if let character = character as? BodyTrackedEntity {
                // Set scale model
                character.scale = [1, 1, 1]
                self.character = character
                cancellable?.cancel()
            } else {
                print("Error: Unable to load model as BodyTrackedEntity")
            }
            
        })
        
    }
    
    func session(_ session: ARSession, didUpdate anchors: [ARAnchor]) {
        for anchor in anchors {
            guard let bodyAnchor = anchor as? ARBodyAnchor else { return }
            
            // Update position of model
            let bodyPosition = simd_make_float3(bodyAnchor.transform.columns.3)
            characterAnchor.position = bodyPosition + characterOffset
            
            // Rotate model if body anchor was rotated
            characterAnchor.orientation = Transform(matrix: bodyAnchor.transform).rotation
            
            // When body anchor detected, load model
            if let character = character, character.parent == nil {
                characterAnchor.addChild(character)
            }
            
            // Root node position
            let hipWorldPosition = bodyAnchor.transform
            print(hipWorldPosition)
            
            // Skeleton geometry
            let skeleton = bodyAnchor.skeleton
            
            // Relative to parent
            let headLocalTransforms = skeleton.localTransform(for: .head)
            print("Head Local : \(headLocalTransforms?.determinant ?? 0)")
            // Relative to root
            let headRootTransforms = skeleton.modelTransform(for: .head)
            print("Head Root : \(headRootTransforms?.determinant ?? 0)")
            
            
            // List of transforms of all joint relative to Root
            let jointTransforms = skeleton.jointModelTransforms
            // Iterate all over join
            for (i, jointTransform) in jointTransforms.enumerated() {
                let parentIndex = skeleton.definition.parentIndices[ i ]
                guard parentIndex != -1 else { continue }
//                let parentJointTransform = jointTransforms[parentIndex.hashValue]
                
                
//                print("Joint Transform : \(jointTransform.debugDescription)")
//                print("Parent Joint Transform \(parentJointTransform.debugDescription)")
            }
        }
    }
}
